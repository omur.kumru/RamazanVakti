//
//  MonthlyViewController.swift
//  Ramazan 2017
//
//  Created by Ömür Kumru on 25.05.2017.
//  Copyright © 2017 Ömür Kumru. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMobileAds

class MonthlyViewController: UIViewController, UITabBarDelegate,UITableViewDataSource {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var aylıkBanner: GADBannerView!
    
    var tarihString = [String]()
    var iftarString = [String]()
    var sahurString = [String]()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarihString.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.dateCellLabel.text = tarihString[indexPath.row]
        cell.sahurCellLabel.text = sahurString[indexPath.row]
        cell.iftarCellLabel.text = iftarString[indexPath.row]
        
        return cell
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        aylıkBanner.adUnitID = "ca-app-pub-3329588112236907/1139255278"
        aylıkBanner.rootViewController = self
        aylıkBanner.load(GADRequest())
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let getCityNameSet = UserDefaults.standard
        let cityName : String = getCityNameSet.string(forKey: "cityNameKey")!
        let cityUrl : String = getCityNameSet.string(forKey: "cityUrlKey")!
        let requestUrl = "http://www.namazvaktim.net/json/aylik/\(cityUrl).json"
        navigationBar.topItem?.title = cityName
        
        Alamofire.request(requestUrl).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if self.tarihString.isEmpty == true {
                    for i in 0...(swiftyJsonVar["namazvakitleri"]["vakitler"].count-1) {
                        
                        self.tarihString.append(swiftyJsonVar["namazvakitleri"]["vakitler"][i]["tarih"].string!)
                        self.sahurString.append(swiftyJsonVar["namazvakitleri"]["vakitler"][i]["imsak"].string!)
                        self.iftarString.append(swiftyJsonVar["namazvakitleri"]["vakitler"][i]["aksam"].string!)
                    }
                    
                }
                
                self.tableView.reloadData()
                
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



