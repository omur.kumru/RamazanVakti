//
//  SelectedLocationViewController.swift
//  Ramazan 2017
//
//  Created by Ömür Kumru on 24.05.2017.
//  Copyright © 2017 Ömür Kumru. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Toast_Swift
import UserNotifications
import GoogleMobileAds

class SelectedLocationViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    let alarmTimes = ["0","15","30","45","60"]
    
    var sahurStringText = ""
    var iftarStringText = ""
    
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var Swit: UISwitch!
    @IBOutlet weak var bildirimText: UILabel!
    @IBOutlet weak var dkOnceText: UILabel!
    @IBOutlet weak var iftarTime: UILabel!
    @IBOutlet weak var sahurTime: UILabel!
    @IBOutlet weak var textsahur: UILabel!
    @IBOutlet weak var dateNormal: UILabel!
    @IBOutlet weak var textiftar: UILabel!
    @IBOutlet weak var navigationTitle: UINavigationBar!
    @IBOutlet weak var dateArabic: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var selectedBanner: GADBannerView!
    
    
    @IBAction func swCo(_ sender: UISwitch) {
        
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        
        if notificationType?.rawValue == 0 {
            Swit.setOn(false, animated: true)
            self.view.makeToast("Bildirimleri kullanabilmek için izin vermeniz gerekmektedir", duration: 1.5, position: .center)
            
            
        } else {
            if sender.isOn == true {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat =  "HH:mm"
                
                var datePickerCurrent = dateFormatter.string(from: datePicker.date)
                
                
                
                self.view.makeToast("Bildirim saat \(datePickerCurrent) için kuruldu", duration: 1.5, position: .center)
                notificationTimer(hour: datePickerCurrent)
            }else{
                
                self.view.makeToast("Bildirim kaldırıldı", duration: 1.5, position: .center)
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            }
            
            print("Enabled")
        }
    }
    
    
    @IBAction func segControl(_ sender: UISegmentedControl)
    {
        
        if segmentedController.selectedSegmentIndex == 0 {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            
            var row = pickerView.selectedRow(inComponent: 0)
            
            var dateSahur = dateFormatter.date(from: sahurStringText)
            
            dateSahur?.addTimeInterval(TimeInterval(Double(alarmTimes[row])!*(-60)))
            
            let sahurString: String = dateFormatter.string(from: dateSahur!)
            
            let date = dateFormatter.date(from: sahurString)
            
            datePicker.date = date!
            
            
        }
        
        if segmentedController.selectedSegmentIndex == 1 {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            
            var row = pickerView.selectedRow(inComponent: 0)
            
            var dateIftar = dateFormatter.date(from: iftarStringText)
            
            dateIftar?.addTimeInterval(TimeInterval(Double(alarmTimes[row])!*(-60)))
            
            let iftarString: String = dateFormatter.string(from: dateIftar!)
            
            let date = dateFormatter.date(from: iftarString)
            
            datePicker.date = date!
            
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.backgroundColor = UIColor.white
        
        datePicker.layer.masksToBounds = true
        datePicker.layer.cornerRadius = 10
        
        
        textsahur.layer.masksToBounds = true
        textsahur.layer.cornerRadius = 10
        
        textiftar.layer.masksToBounds = true
        textiftar.layer.cornerRadius = 10
        
        iftarTime.layer.masksToBounds = true
        iftarTime.layer.cornerRadius = 10
        
        sahurTime.layer.masksToBounds = true
        sahurTime.layer.cornerRadius = 10
        
        bildirimText.layer.masksToBounds = true
        bildirimText.layer.cornerRadius = 10
        
        selectedBanner.adUnitID = "ca-app-pub-3329588112236907/1139255278"
        selectedBanner.rootViewController = self
        selectedBanner.load(GADRequest())
        
        let dateFor = DateFormatter()
        
        let hijriCalendar = Calendar.init(identifier: Calendar.Identifier.islamicCivil)
        dateFor.locale = Locale.init(identifier: "en")
        dateFor.calendar = hijriCalendar
        
        dateFor.dateFormat = "dd MMMM yyyy"
        
        
        dateArabic.text = dateFor.string(from: Date())
        
        
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd MMMM yyyy"
        
        let dateResult = formatter.string(from: date)
        
        dateNormal.text = dateResult
        
        //navigationTitle.topItem?.title = cityNameGet
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
        })
        
        let getCityNameSet = UserDefaults.standard
        let cityName : String = getCityNameSet.string(forKey: "cityNameKey")!
        let cityUrl : String = getCityNameSet.string(forKey: "cityUrlKey")!
        
        navigationTitle.topItem?.title = cityName
        
        Alamofire.request("http://www.namazvaktim.net/json/gunluk/\(cityUrl).json").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                self.sahurStringText = swiftyJsonVar["namazvakitleri"]["zaman"]["vakitler"]["imsak"].string!
                self.iftarStringText = swiftyJsonVar["namazvakitleri"]["zaman"]["vakitler"]["aksam"].string!
                
                self.sahurTime.text = self.sahurStringText
                self.iftarTime.text = self.iftarStringText
                self.datePicker.date = self.datePickerSetter(hour: swiftyJsonVar["namazvakitleri"]["zaman"]["vakitler"]["imsak"].string!)
                
            }
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return alarmTimes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return alarmTimes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if segmentedController.selectedSegmentIndex == 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            var dateSahur = dateFormatter.date(from: sahurStringText)
            dateSahur?.addTimeInterval(TimeInterval(Double(alarmTimes[row])!*(-60)))
            let sahurString: String = dateFormatter.string(from: dateSahur!)
            let date = dateFormatter.date(from: sahurString)
            datePicker.date = date!
            
        }
        
        if segmentedController.selectedSegmentIndex == 1 {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            
            var row = pickerView.selectedRow(inComponent: 0)
            
            var dateIftar = dateFormatter.date(from: iftarStringText)
            
            dateIftar?.addTimeInterval(TimeInterval(Double(alarmTimes[row])!*(-60)))
            
            let iftarString: String = dateFormatter.string(from: dateIftar!)
            
            let date = dateFormatter.date(from: iftarString)
            
            datePicker.date = date!
            
        }
    }
    
    func datePickerSetter(hour: String) ->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        let date = dateFormatter.date(from: hour)
        return date!
    }
    
    func notificationTimer(hour: String) {
        
        
        let content = UNMutableNotificationContent()
        content.title = "Ramazan 2017"
        content.subtitle = "\(hour) için kurduğun alarm"
        content.body = "Allah kabul etsin"
        content.badge = 1
        content.sound = UNNotificationSound.default()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        let dateClock = dateFormatter.date(from: hour)
        
        
        let calendar = Calendar.current
        let hoursComp = calendar.component(.hour, from: dateClock!)
        let minutesComp = calendar.component(.minute, from: dateClock!)
        
        var date = DateComponents()
        date.hour = hoursComp
        date.minute = minutesComp
        let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: false)
        
        
        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
}


