//
//  EnterViewController.swift
//  Ramazan 2017
//
//  Created by Ömür Kumru on 25.05.2017.
//  Copyright © 2017 Ömür Kumru. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Toast_Swift
import GoogleMobileAds

class EnterViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var otomatikbtnLabel: UIButton!
    @IBOutlet weak var elleButonLabel: UIButton!
    @IBOutlet weak var ramazan2017Label: UILabel!
    @IBOutlet weak var konumsecimiLabel: UILabel!
    @IBOutlet weak var enterBanner: GADBannerView!
    
    var cityUrlOtomatik = "istanbul"
    var cityNameOtomatik = "İstanbul"
    
    let manager = CLLocationManager()
    @IBAction func otomatikClicked(_ sender: Any) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.view.makeToast("Bu özellik için konum kullanıma izin vermeniz gerekmektedir", duration: 1.5, position: .center)
            case .authorizedAlways, .authorizedWhenInUse:
                let cityNameSet = UserDefaults.standard
                cityNameSet.set(cityNameOtomatik, forKey: "cityNameKey")
                cityNameSet.set(cityUrlOtomatik, forKey: "cityUrlKey")
                performSegue(withIdentifier: "otoSegue", sender: self)
            }
        } else {
            print("Location services are not enabled")
            
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            
            
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemark, error) in
                
                
                if error != nil
                {
                    print("error")
                }else{
                    if let place = placemark?[0]{
                        
                        
                        if let city = place.addressDictionary?["State"] as? NSString
                        {
                            
                            self.cityUrlOtomatik = self.changeToEnglishCharacter(target: city as String)
                            self.cityNameOtomatik = city as String
                            
                        }
                        
                    }
                }
                
            })
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    func changeToEnglishCharacter (target: String) -> String{
        
        var swiftyString = target.lowercased()
        swiftyString = swiftyString.replacingOccurrences(of: "ğ", with: "g")
        swiftyString = swiftyString.replacingOccurrences(of: "ı", with: "i")
        swiftyString = swiftyString.replacingOccurrences(of: "ö", with: "o")
        swiftyString = swiftyString.replacingOccurrences(of: "ü", with: "u")
        swiftyString = swiftyString.replacingOccurrences(of: "ç", with: "c")
        swiftyString = swiftyString.replacingOccurrences(of: "ş", with: "s")
        
        return swiftyString
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.requestLocation()
        
        enterBanner.adUnitID = "ca-app-pub-3329588112236907/1139255278"
        enterBanner.rootViewController = self
        enterBanner.load(GADRequest())
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
