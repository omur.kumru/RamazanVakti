//
//  LocationView.swift
//  Ramazan 2017
//
//  Created by Ömür Kumru on 23.05.2017.
//  Copyright © 2017 Ömür Kumru. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMobileAds

var citySendUrl = ""
var citySendName = ""

class LocationView: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
  
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var locationBanner: GADBannerView!
    var cityName = [String]()
    var cityUrl = [String]()
    

    @IBAction func locationButton(_ sender: Any)
    {
        citySendUrl = cityUrl[pickerView.selectedRow(inComponent: 0)]
        citySendName = cityName[pickerView.selectedRow(inComponent: 0)]
        
        
        let cityNameSet = UserDefaults.standard
        cityNameSet.set(citySendName, forKey: "cityNameKey")
        cityNameSet.set(citySendUrl, forKey: "cityUrlKey")
        
    }
    
    override func  viewDidLoad() {
        super.viewDidLoad()
        
        
        Alamofire.request("http://www.namazvaktim.net/json/sehirler/turkiye.json").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                var NameCity = [String]()
                var UrlCity = [String]()
                
                for i in 0...(swiftyJsonVar["cities"].count-1) {
                    
                    NameCity.append(swiftyJsonVar["cities"][i]["name"].string!)
                    UrlCity.append(swiftyJsonVar["cities"][i]["url"].string!)
                    
                }
                
                self.cityName = NameCity
                self.cityUrl = UrlCity
                self.pickerView.reloadAllComponents()
                self.locationBanner.adUnitID = "ca-app-pub-3329588112236907/1139255278"
                self.locationBanner.rootViewController = self
                self.locationBanner.load(GADRequest())
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cityName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityName.count
        
    }
    
    
    
    
}
